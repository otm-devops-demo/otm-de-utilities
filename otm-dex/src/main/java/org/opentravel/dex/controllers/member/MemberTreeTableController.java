/**
 * Copyright (C) 2014 OpenTravel Alliance (info@opentravel.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.opentravel.dex.controllers.member;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.application.common.events.AbstractOtmEvent;
import org.opentravel.common.cellfactories.ValidationMemberTreeTableCellFactory;
import org.opentravel.dex.controllers.DexController;
import org.opentravel.dex.controllers.DexIncludedControllerBase;
import org.opentravel.dex.controllers.DexMainController;
import org.opentravel.dex.events.DexFilterChangeEvent;
import org.opentravel.dex.events.DexMemberDeleteEvent;
import org.opentravel.dex.events.DexMemberSelectionEvent;
import org.opentravel.dex.events.DexModelChangeEvent;
import org.opentravel.dex.events.OtmObjectChangeEvent;
import org.opentravel.dex.events.OtmObjectModifiedEvent;
import org.opentravel.dex.events.OtmObjectReplacedEvent;
import org.opentravel.model.OtmChildrenOwner;
import org.opentravel.model.OtmModelManager;
import org.opentravel.model.otmFacets.OtmAbstractDisplayFacet;
import org.opentravel.model.otmFacets.OtmContributedFacet;
import org.opentravel.model.otmFacets.OtmEmptyTableFacet;
import org.opentravel.model.otmLibraryMembers.OtmLibraryMember;
import org.opentravel.objecteditor.ObjectEditorController;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.SortType;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * Manage the library member navigation tree.
 * 
 * @author dmh
 *
 */
public class MemberTreeTableController extends DexIncludedControllerBase<OtmModelManager> implements DexController {
    private static Log log = LogFactory.getLog( MemberTreeTableController.class );

    // Column labels
    // TODO - externalize strings
    public static final String PREFIXCOLUMNLABEL = "Prefix";
    private static final String NAMECOLUMNLABEL = "Member";
    private static final String VERSIONCOLUMNLABEL = "Version";
    private static final String LIBRARYLABEL = "Library";
    // private static final String ERRORLABEL = "Errors";
    private static final String WHEREUSEDLABEL = "Types Used";

    /*
     * FXML injected
     */
    @FXML
    TreeTableView<MemberAndProvidersDAO> memberTree;
    @FXML
    private VBox memberTreeController;

    //
    TreeItem<MemberAndProvidersDAO> root; // Root of the navigation tree. Is displayed.
    TreeTableColumn<MemberAndProvidersDAO,String> nameColumn; // an editable column

    OtmModelManager currentModelMgr; // this is postedData

    MemberFilterController filter = null;

    private boolean ignoreEvents = false;
    // By default, the tree is editable. Setting this to false will prevent edits.
    private boolean treeEditingEnabled = true;

    // All event types listened to by this controller's handlers
    // Object events may change validation state of members
    private static final EventType[] subscribedEvents = {DexFilterChangeEvent.FILTER_CHANGED,
        DexMemberDeleteEvent.MEMBER_DELETED, OtmObjectReplacedEvent.OBJECT_REPLACED,
        DexMemberSelectionEvent.TYPE_USER_SELECTED, DexMemberSelectionEvent.TYPE_PROVIDER_SELECTED,
        DexMemberSelectionEvent.MEMBER_SELECTED, DexModelChangeEvent.MODEL_CHANGED, OtmObjectChangeEvent.OBJECT_CHANGED,
        OtmObjectModifiedEvent.OBJECT_MODIFIED};
    private static final EventType[] publishedEvents = {DexMemberSelectionEvent.MEMBER_SELECTED};

    /**
     * Construct a member tree table controller that can publish and receive events.
     */
    public MemberTreeTableController() {
        super( subscribedEvents, publishedEvents );
    }

    /**
     * Create columns
     */
    private void buildColumns() {

        TreeTableColumn<MemberAndProvidersDAO,String> prefixColumn = new TreeTableColumn<>( PREFIXCOLUMNLABEL );
        prefixColumn.setCellValueFactory( new TreeItemPropertyValueFactory<MemberAndProvidersDAO,String>( "prefix" ) );
        setColumnProps( prefixColumn, true, false, true, 100 );
        prefixColumn.setStyle( "-fx-alignment: CENTER-RIGHT;" );

        nameColumn = new TreeTableColumn<>( NAMECOLUMNLABEL );
        nameColumn.setCellValueFactory( new TreeItemPropertyValueFactory<MemberAndProvidersDAO,String>( "name" ) );
        setColumnProps( nameColumn, true, true, true, 200 );
        nameColumn.setSortType( SortType.ASCENDING );

        TreeTableColumn<MemberAndProvidersDAO,String> versionColumn = new TreeTableColumn<>( VERSIONCOLUMNLABEL );
        versionColumn
            .setCellValueFactory( new TreeItemPropertyValueFactory<MemberAndProvidersDAO,String>( "version" ) );

        TreeTableColumn<MemberAndProvidersDAO,String> libColumn = new TreeTableColumn<>( LIBRARYLABEL );
        libColumn.setCellValueFactory( new TreeItemPropertyValueFactory<MemberAndProvidersDAO,String>( "library" ) );

        TreeTableColumn<MemberAndProvidersDAO,String> usedTypesCol = new TreeTableColumn<>( WHEREUSEDLABEL );
        usedTypesCol
            .setCellValueFactory( new TreeItemPropertyValueFactory<MemberAndProvidersDAO,String>( "usedTypes" ) );

        TreeTableColumn<MemberAndProvidersDAO,ImageView> valColumn = new TreeTableColumn<>( "" );
        valColumn.setCellFactory( c -> new ValidationMemberTreeTableCellFactory() );
        setColumnProps( valColumn, true, false, false, 25 );

        // Add columns to table
        memberTree.getColumns().addAll( nameColumn, valColumn, libColumn, versionColumn, prefixColumn, usedTypesCol );
        memberTree.getSortOrder().add( nameColumn );
    }

    @Override
    public void checkNodes() {
        if (memberTree == null)
            throw new IllegalStateException( "Tree table view is null." );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        if (memberTree.getSelectionModel() != null)
            memberTree.getSelectionModel().clearSelection();

        if (memberTree.getRoot() != null)
            memberTree.getRoot().getChildren().clear();
    }

    /**
     * Configure the controller for use by main controller.
     */
    @Override
    public void configure(DexMainController parent) {
        super.configure( parent );
        // log.debug("Configuring Member Tree Table.");
        eventPublisherNode = memberTreeController;
        configure( parent.getModelManager(), treeEditingEnabled );
    }

    /**
     * Configure controller for use by non-main controllers.
     * 
     * @param modelMgr must not be null
     * @param editable sets tree editing enables
     */
    public void configure(OtmModelManager modelMgr, boolean editable) {
        if (modelMgr == null)
            throw new IllegalArgumentException(
                "Model manager is null. Must configure member tree with model manager." );

        this.currentModelMgr = modelMgr;
        this.treeEditingEnabled = editable;

        // Set the hidden root item
        root = new TreeItem<>();
        root.setExpanded( true ); // Startout fully expanded

        // Set up the TreeTable
        memberTree.setRoot( getRoot() );
        memberTree.setShowRoot( false );
        memberTree.setEditable( true );
        memberTree.getSelectionModel().setCellSelectionEnabled( true ); // allow individual cells to be edited
        memberTree.setTableMenuButtonVisible( true ); // allow users to select columns
        // Enable context menus at the row level and add change listener for for applying style
        memberTree.setRowFactory( (TreeTableView<MemberAndProvidersDAO> p) -> new MemberRowFactory( this ) );
        buildColumns();

        // Add listeners and event handlers
        memberTree.getSelectionModel().select( 0 );
        memberTree.setOnKeyReleased( this::keyReleased );
        // memberTree.setOnMouseClicked(this::mouseClick);
        memberTree.getSelectionModel().selectedItemProperty()
            .addListener( (v, old, newValue) -> memberSelectionListener( newValue ) );

        refresh();
    }

    /**
     * Note: TreeItem class does not extend the Node class. Therefore, you cannot apply any visual effects or add menus
     * to the tree items. Use the cell factory mechanism to overcome this obstacle and define as much custom behavior
     * for the tree items as your application requires.
     * 
     * @param member the Otm Library Member to add to the tree
     * @param parent the tree root or parent member
     * @return
     */
    public void createTreeItem(OtmLibraryMember member, TreeItem<MemberAndProvidersDAO> parent) {
        // log.debug( "Creating member tree item for: " + member + " of type " + member.getClass().getSimpleName() );

        // Apply Filter
        if (filter != null && !filter.isSelected( member ))
            return;

        // 1/3/2020 - let the CFs be shown, the users expect to see them
        // Skip over contextual facets that have been injected into an object. Their contributed facets will be modeled.
        // if (member instanceof OtmContextualFacet && ((OtmContextualFacet) member).getWhereContributed() != null)
        // return;

        // Create item for the library member
        TreeItem<MemberAndProvidersDAO> item = new MemberAndProvidersDAO( member ).createTreeItem( parent );

        // Create and add items for children
        if (member instanceof OtmChildrenOwner)
            createChildrenItems( member, item );
    }

    public void createTreeItem(OtmAbstractDisplayFacet member, TreeItem<MemberAndProvidersDAO> parent) {
        new MemberAndProvidersDAO( member ).createTreeItem( parent );
    }

    /**
     * Create tree items for the type provider children of this child owning member
     */
    private void createChildrenItems(OtmChildrenOwner member, TreeItem<MemberAndProvidersDAO> parentItem) {
        member.getChildrenTypeProviders().forEach( p -> {
            TreeItem<MemberAndProvidersDAO> cfItem = new MemberAndProvidersDAO( p ).createTreeItem( parentItem );

            // Only use contextual facet for recursing
            if (p instanceof OtmContributedFacet && ((OtmContributedFacet) p).getContributor() != null)
                p = ((OtmContributedFacet) p).getContributor();

            // Recurse
            if (p instanceof OtmChildrenOwner)
                createChildrenItems( (OtmChildrenOwner) p, cfItem );
        } );
    }

    public MemberFilterController getFilter() {
        return filter;
    }

    public TreeItem<MemberAndProvidersDAO> getRoot() {
        return root;
    }

    public MemberAndProvidersDAO getSelected() {
        return memberTree.getSelectionModel().getSelectedItem() != null
            ? memberTree.getSelectionModel().getSelectedItem().getValue() : null;
    }

    private void handleEvent(DexFilterChangeEvent event) {
        if (!ignoreEvents)
            refresh();
    }


    @Override
    public void handleEvent(AbstractOtmEvent event) {
        // log.debug( event.getEventType() + " event received. Ignore? " + ignoreEvents );
        if (!ignoreEvents) {
            if (event instanceof DexMemberSelectionEvent)
                handleEvent( (DexMemberSelectionEvent) event );
            else if (event instanceof DexFilterChangeEvent)
                handleEvent( (DexFilterChangeEvent) event );
            else if (event instanceof OtmObjectChangeEvent)
                refresh();
            else if (event instanceof OtmObjectReplacedEvent)
                handleEvent( (OtmObjectReplacedEvent) event );
            else if (event instanceof DexMemberDeleteEvent)
                refresh();
            else if (event instanceof DexModelChangeEvent)
                refresh();
            else
                refresh();
        }
    }

    private void handleEvent(OtmObjectReplacedEvent event) {
        if (event.getOrginalObject().getOwningMember() == event.getReplacementObject().getOwningMember())
            log.error( "BAD HERE" );
        refresh();
        if (event.getOrginalObject().getOwningMember() == event.getReplacementObject().getOwningMember())
            log.error( "BAD HERE" );
    }

    private void handleEvent(DexMemberSelectionEvent event) {
        if (event.getEventType().equals( DexMemberSelectionEvent.RESOURCE_SELECTED ))
            return;
        if (!ignoreEvents)
            select( event.getMember() );
    }

    public void keyReleased(KeyEvent event) {
        TreeItem<MemberAndProvidersDAO> item = memberTree.getSelectionModel().getSelectedItem();
        int row = memberTree.getSelectionModel().getSelectedIndex();
        if (item == null)
            return;

        // log.debug("Selection row = " + row);
        if (event.getCode() == KeyCode.RIGHT) {
            event.consume();
            item.setExpanded( true );
            memberTree.getSelectionModel().clearAndSelect( row + 1, nameColumn );
        } else if (event.getCode() == KeyCode.LEFT) {
            TreeItem<MemberAndProvidersDAO> parent = item.getParent();
            if (parent != null && parent != item && parent != root) {
                memberTree.getSelectionModel().select( parent );
                parent.setExpanded( false );
                row = memberTree.getSelectionModel().getSelectedIndex();
                memberTree.getSelectionModel().clearAndSelect( row, nameColumn );
                event.consume();
            }
        }
    }

    /**
     * Listener for selected library members in the tree table.
     * 
     * @param item
     */
    private void memberSelectionListener(TreeItem<MemberAndProvidersDAO> item) {
        if (item == null)
            return;
        // log.debug("Selection Listener: " + item.getValue());
        assert item != null;
        boolean editable = false;
        if (treeEditingEnabled && item.getValue() != null)
            editable = item.getValue().isEditable();
        nameColumn.setEditable( editable ); // TODO - is this still useful?
        ignoreEvents = true;
        if (eventPublisherNode != null)
            eventPublisherNode.fireEvent( new DexMemberSelectionEvent( this, item ) );
        ignoreEvents = false;
    }

    public void mouseClick(MouseEvent event) {
        // this fires after the member selection listener
        // if (event.getButton().equals( MouseButton.PRIMARY ) && event.getClickCount() == 2)
        // log.debug( "Double click selection: " );
        // + memberTree.getSelectionModel().getSelectedItem().getValue().nameProperty().toString());
    }

    /**
     * Get the library members from the model manager and put them into a cleared tree.
     * 
     * @param modelMgr
     */
    @Override
    public void post(OtmModelManager modelMgr) {
        ignoreEvents = true;
        if (modelMgr != null && memberTree != null) {
            currentModelMgr = modelMgr;
            clear();

            // Get the filter to use if needed
            if (filter == null && getMainController() instanceof ObjectEditorController)
                filter = ((ObjectEditorController) getMainController()).getMemberFilterController();

            // create cells for members
            currentModelMgr.getMembers().forEach( m -> createTreeItem( m, root ) );

            // If no members, post an empty row to allow row factory to add menu items
            if (root.getChildren().isEmpty())
                createTreeItem( new OtmEmptyTableFacet( currentModelMgr ), root );

            try {
                memberTree.sort();
            } catch (Exception e) {
                log.warn( "Exception sorting: " + e.getLocalizedMessage() );
            }
        }
        ignoreEvents = false;
        // log.debug( "Posted member tree." );
    }

    /**
     * {@inheritDoc} Clear the tree and post the model.
     * 
     * @see #post(OtmModelManager)
     * @see org.opentravel.dex.controllers.DexIncludedControllerBase#refresh()
     */
    @Override
    public void refresh() {
        post( currentModelMgr );
    }

    public void select(OtmLibraryMember otm) {
        memberTree.getSelectionModel().clearSelection();
        if (otm != null) {
            for (TreeItem<MemberAndProvidersDAO> item : memberTree.getRoot().getChildren()) {
                if (item.getValue().getValue() == otm) {
                    int row = memberTree.getRow( item );
                    // This may not highlight the row if the event comes from or goes to a different controller.
                    Platform.runLater( () -> {
                        // ignoreEvents = true;
                        memberTree.requestFocus();
                        memberTree.getSelectionModel().clearAndSelect( row );
                        memberTree.scrollTo( row );
                        memberTree.getFocusModel().focus( row );
                        // ignoreEvents = false;
                    } );
                    // log.debug("Selected " + otm.getName() + " in member tree.");
                    return;
                }
            }
            // log.warn( otm.getName() + " not found in member tree." );
        }
    }

    public void setFilter(MemberFilterController filter) {
        this.filter = filter;
    }

    public void setOnMouseClicked(EventHandler<? super MouseEvent> handler) {
        memberTree.setOnMouseClicked( handler );
    }
}
